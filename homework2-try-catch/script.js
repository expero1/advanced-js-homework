"use strict";
const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

const REQUIRED_BOOK_INFO_FIELDS = ["author", "name", "price"];

class BookInfoError extends Error {
  constructor(bookInfo, fieldName) {
    super();
    this.name = "BookInfoError";
    this.message = `Book not include required field "${fieldName}"\nChecked Book: ${JSON.stringify(
      bookInfo
    )}`;
    this.bookInfo = bookInfo;
    this.fieldName = fieldName;
  }
}
function checkRequiredFields(bookInfo, requiredBookInfoFields) {
  requiredBookInfoFields.forEach((fieldName) => {
    // nonExistentVariable;
    if (!(fieldName in bookInfo)) {
      throw new BookInfoError(bookInfo, fieldName);
    }
  });
  return true;
}
function filterBooks(books) {
  return books.filter((bookInfo) => {
    try {
      return checkRequiredFields(bookInfo, REQUIRED_BOOK_INFO_FIELDS);
    } catch (error) {
      if (error instanceof BookInfoError) {
        console.log(error);
        // console.log(JSON.stringify(error.bookInfo));
      } else throw error;
    }
  });
}
function createBookCard(bookInfo) {
  // let bookCard = "";
  // for (let fieldName in bookInfo) {
  //   bookCard += `<p>${bookInfo[fieldName]}</p>`;
  // }
  return `<li>${JSON.stringify(bookInfo)}</li>`;
}

function showBooks(books) {
  const rootElement = document.querySelector("#root");
  const booksToShow = filterBooks(books);
  const bookCards = booksToShow.map((book) => createBookCard(book));
  rootElement.innerHTML = `<ul>${bookCards.join("")}</ul>`;
}
showBooks(books);

/*
Дано масив books.

const books = [
  { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
  }, 
  {
   author: "Сюзанна Кларк",
   name: "Джонатан Стрейндж і м-р Норрелл",
  }, 
  { 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  }, 
  { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  }, 
  {
   author: "Террі Пратчетт",
   name: "Рухомі картинки",
   price: 40
  },
  {
   author: "Анґус Гайленд",
   name: "Коти в мистецтві",
  }
];

- Виведіть цей масив на екран у вигляді списку (тег ul – список має бути 
згенерований за допомогою Javascript).

- На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати 
цей список (схоже завдання виконувалось в модулі basic).

- Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність 
(в об'єкті повинні міститися всі три властивості - author, name, price). 
Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із 
зазначенням - якої властивості немає в об'єкті.

- Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.

Примітка
Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.



*/
