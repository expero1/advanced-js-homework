"use strict";
const API = {
  ipify: "https://api.ipify.org/?format=json",
  ipApi: (ip) =>
    `http://ip-api.com/json/${ip}?fields=status,message,continent,country,regionName,city,district,query`,
};
const startButtonClassName = ".get-ip-info-btn";
const loaderClassName = ".loader";
const ipInfoClassName = ".ip-info";
const messageClassName = ".messages";

const ipApiSuccessStatus = "success";

const capitalizeFirstLetter = (word) =>
  word.charAt(0).toUpperCase() + word.slice(1);

class CustomError extends Error {
  constructor(message, error, context) {
    super(message);
    this.name = "CustomError";
    this.context = context;
    this.stack = error?.stack;
  }
}
const showMessage = (message = "") => {
  const messageContainer = document.querySelector(messageClassName);
  messageContainer.innerText = message;
};

const handleError = (error) => {
  showMessage(error.message);
  console.log(error);
  if (error?.context) console.log(error.context);
};

const fetchData = async (url) => {
  const response = await fetch(url);
  if (!response.ok)
    throw new CustomError(`Response error,`, undefined, {
      status: response.status,
      url: response.url,
    });

  const json = await response.json().catch((error) => {
    throw new CustomError("JSON error", error, {
      url,
      responseBody: response,
    });
  });

  return json;
};

const getIp = async () => {
  const ip = await fetchData(API.ipify);
  return ip.ip;
};

const getIpInfo = async (ip) => {
  const ipInfo = await fetchData(API.ipApi(ip));
  return ipInfo;
};

const renderIpInfo = (info) => {
  let ipInfo = "";
  for (let [key, value] of Object.entries(info)) {
    if (value.length > 0)
      ipInfo += `<p>${capitalizeFirstLetter(key)}: ${value}</p>`;
  }
  return `<div>${ipInfo}</div>`;
};
const showIpInfo = async () => {
  const loader = document.querySelector(loaderClassName);
  const ipInfo = document.querySelector(ipInfoClassName);
  ipInfo.innerText = "";
  loader.classList.toggle("active");
  showMessage("");
  try {
    const ip = await getIp();
    const {
      status,
      message = "",
      continent,
      country,
      regionName: region,
      city,
      district,
    } = await getIpInfo(ip);
    if (status !== ipApiSuccessStatus)
      throw new CustomError(`Server Response Error`, undefined, {
        status,
        message,
      });
    ipInfo.innerHTML = renderIpInfo({
      continent,
      country,
      region,
      city,
      district,
    });
  } catch (error) {
    handleError(error, false);
  }
  loader.classList.toggle("active");
};

const button = document.querySelector(startButtonClassName);
button.addEventListener("click", showIpInfo);
// getIpApi();
/*
Написати програму "Я тебе знайду по IP"

Технічні вимоги:

- Створити просту HTML-сторінку з кнопкою Знайти по IP.
- Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
- Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
- під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
- Усі запити на сервер необхідно виконати за допомогою async await.


Примітка
Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.
*/
