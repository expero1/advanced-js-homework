import { getUser } from "../tasks.js";
import { getPosts } from "../remote-tasks.js";
import { Card } from "./card.js";
import { CustomHTMLEement } from "./custom-html-element.js";
import { Loader } from "./loader.js";

export class Posts extends CustomHTMLEement {
  constructor() {
    super();
    this.html = `<div class='posts'></div>`;
    this.renderTree();
  }
  renderTree() {
    const pageLoader = new Loader("page-loader");
    this.addElementToTree(pageLoader);
    getPosts()
      .then((posts) => {
        posts.forEach((post) => {
          getUser(post.userId).then((user) => {
            const card = new Card({
              postBody: post.body,
              postTitle: post.title,
              userName: user.name,
              name: user.username,
              userId: user.id,
              postId: post.id,
            });
            this.addElementToTree(card, this, "beforeend");
          });
        });
      })
      .finally(() => {
        pageLoader.remove();
      });
  }
}
