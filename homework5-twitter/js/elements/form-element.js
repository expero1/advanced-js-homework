import { CustomHTMLEement } from "./custom-html-element.js";
import { PostEditError } from "./post-edit-error-list.js";

export class FormElement extends CustomHTMLEement {
  formToObject() {
    return Object.fromEntries(new FormData(this.rendered));
  }
  addErrorMessage(message, inputName) {
    this.addElementToTree(
      new PostEditError(message),
      this.getChildElement(`[name=${inputName}]`),
      "afterend"
    );
  }
}
