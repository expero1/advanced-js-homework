import { getPost, getUser, showMessage } from "../tasks.js";
import { deletePost } from "../remote-tasks.js";
import { CustomHTMLEement } from "./custom-html-element.js";
import { Loader } from "./loader.js";
import { PostForm } from "./post-form.js";
import { ModalWindow } from "./modal-window.js";
export class Card extends CustomHTMLEement {
  constructor({ postBody, postTitle, userName, name, userId, postId }) {
    super({ postBody, postTitle, userName, name, userId, postId });
    this.html = `<div class="post">
    <img src="https://placehold.co/50" class="post__author-logo" alt="${userName}"/>
    <a href="users/${userId}" class="post__author">${this.props.userName}
    <img class="post__verified" src="img/badge.svg"/>
    <span class="post__nickname">@${this.props.name}</span></a>
	  <h2 class="post__title">${this.props.postTitle}</h2>
	  
	  <div class="post__content">${this.props.postBody}</div>
    <div class="post__edit-container">
	  <button class="post__remove-btn">Delete post</button>
    <button class="edit-post">Edit post</button>
    </div>
	  </div>`;
    this.registerEventListener("click", ".post__remove-btn", this.deletePost);
    this.registerEventListener("click", ".edit-post", this.editPost);
  }

  editPost(event) {
    event.preventDefault();
    const loader = new Loader();
    this.addElementToTree(loader);
    getPost(this.props.postId).then(({ id, body, title, userId }) => {
      getUser(userId)
        .then(({ id: userId, name: userName }) => {
          const postEditForm = new PostForm({
            id,
            postBody: body,
            postTitle: title,
            postAuthor: userName,
            userId: userId,
          });
          const modalWindow = new ModalWindow(postEditForm);
          modalWindow.showWindow();
        })
        .finally(() => {
          loader.remove();
        });
    });
  }

  deletePost() {
    const loader = new Loader("post__loader");
    this.addElementToTree(loader);
    deletePost(this.props.postId)
      .then((status) => {
        if (status) {
          this.remove();
          showMessage("Post deleted");
        }
      })
      .finally(() => {
        loader.remove();
      });
  }
}
