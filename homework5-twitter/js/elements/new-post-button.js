import { CustomHTMLEement } from "./custom-html-element.js";
import { ModalWindow } from "./modal-window.js";
import { NewPostForm } from "./new-post-form.js";

export class NewPostButton extends CustomHTMLEement {
  constructor() {
    super();
    this.html = `<button class="edit-post-btn">New Post</button>`;
    this.registerEventListener("click", this, this.showPostEdit);
  }
  showPostEdit() {
    const newPostForm = new NewPostForm();
    const modalWindow = new ModalWindow(newPostForm);
    modalWindow.showWindow();
  }
}
