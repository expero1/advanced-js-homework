import { CustomHTMLEement } from "./custom-html-element.js";

export class RootElement extends CustomHTMLEement {
  constructor() {
    super();
    this.html = `<div class="root"></div>`;
  }
}
