import { renderPosts, showMessage, updatePost } from "../tasks.js";
import { FormElement } from "./form-element.js";
import { Loader } from "./loader.js";
export class PostForm extends FormElement {
  constructor({
    id,
    postBody = "",
    postTitle = "",
    postAuthor = "",
    userId,
  } = {}) {
    super({ id, postBody, postTitle, postAuthor, userId });
    this.html = `
	<form class="new-post__form">
  <input type="text" hidden name="id" value='${id ?? ""}'/>
      <label class="form-label post__title-label">Post title
      <input type="text" class="form__input form__post-title" name="title" value = '${
        this.props.postTitle
      }'/></label>
	<label class="form-label post__body-label">Post body
  <textarea class="form__input form__post-body" name="body" value = ''>${
    this.props.postBody
  }</textarea></label>

	<button class="form__send-btn">send post</button>
	</form>
  `;

    this.registerEventListener("click", ".form__send-btn", this.sendPost);
    this.customEvent;
  }

  sendPost(event) {
    event.preventDefault();

    const formData = this.formToObject(".new-post__form");
    this.clearErrorMessages();
    if (this.checkFormData(formData).status) {
      const loader = new Loader("send-form-loader");
      this.addElementToTree(loader);
      event.target.disabled = true;
      updatePost(formData)
        .then(() => {
          renderPosts();
          showMessage("Post Updated");
          this.sendEvent(this.eventNames.closeModalWindow);
        })
        .finally(() => {
          loader.remove();
          event.target.disabled = false;
        });
    }
  }
  checkFormData(formData) {
    let checkStatus = true;
    if (formData.title.length === 0) {
      this.addErrorMessage("Title must not be empty", "title");
      checkStatus = false;
    }

    if (formData.body.length === 0) {
      this.addErrorMessage("Body must not be empty", "body");
      checkStatus = false;
    }
    return { status: checkStatus };
  }
  clearErrorMessages() {
    this.rendered
      .querySelectorAll(".form__error-messages")
      ?.forEach((element) => element.remove());
  }
}
