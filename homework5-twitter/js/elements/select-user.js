import { CustomHTMLEement } from "./custom-html-element.js";

export class UserSelectInput extends CustomHTMLEement {
  constructor({ users }) {
    super({ users });
    this.html = `
      <label class="form-label form__user-label">Post author
	      <select name="userId" class="user-select" class="form__input" > 
          <option value = "">Please select name of author</option>
        </select> 
      </label>`;
    this.createSelectElement();
  }
  createSelectElement() {
    this.props.users.forEach(({ id, name, username }) =>
      this.addElementToTree(
        this.stringToHtmlElement(
          `<option value = "${id}">${name} (@${username})</option>`
        ),
        ".user-select",
        "beforeend"
      )
    );
  }
}
