import { addPost, renderPosts, showMessage } from "../tasks.js";
import { getUsers } from "../remote-tasks.js";
import { Loader } from "./loader.js";
import { PostForm } from "./post-form.js";
import { UserSelectInput } from "./select-user.js";

export class NewPostForm extends PostForm {
  constructor() {
    super();
    this.createUserSelectElement();
  }
  sendPost(event) {
    event.preventDefault();

    const formData = this.formToObject(".new-post__form");
    this.clearErrorMessages();
    if (this.checkFormData(formData).status) {
      const loader = new Loader("send-form-loader");
      this.addElementToTree(loader);
      event.target.disabled = true;
      addPost(formData)
        .then(() => {
          showMessage("Post Saved");
          showMessage("Post Saved");
          renderPosts();
          this.sendEvent(this.eventNames.closeModalWindow);
        })
        .finally(() => {
          loader.remove();
          event.target.disabled = false;
        });
    }
  }
  createUserSelectElement() {
    const loader = new Loader();
    this.addElementToTree(loader);
    getUsers().then((users) => {
      this.addElementToTree(
        new UserSelectInput({ users }),
        undefined,
        "afterbegin"
      );
      loader.remove();
    });
  }
  checkFormData(formData) {
    let checkStatus = super.checkFormData(formData).status;
    if (!formData.userId) {
      this.addErrorMessage("Please select user", "userId");
      checkStatus = false;
    }
    return { status: checkStatus };
  }
}
