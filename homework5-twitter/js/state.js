import { getPost } from "./tasks.js";

const STATE = {
  users: [],
  posts: [],
};
window.STATE = STATE;
const GET_USERS_ACTION = "get-users-action";
const GET_POSTS_ACTION = "get-posts-action";
const SET_POSTS_ACTION = "set-posts-action";
const SET_USERS_ACTION = "set-users-action";
const GET_POST_ACTION = "get-post-action";
const ADD_POST_ACTION = "add-post-action";
const UPDATE_POST_ACTION = "update-post-action";
export const getUsersAction = () => {
  return STATE_REDUCER(GET_USERS_ACTION);
};
export const setUsersAction = ({ usersInfo }) => {
  return STATE_REDUCER(SET_USERS_ACTION, { usersInfo });
};
export const getPostsAction = () => {
  return STATE_REDUCER(GET_POSTS_ACTION);
};
export const setPostsAction = ({ postsInfo }) => {
  STATE_REDUCER(SET_POSTS_ACTION, { postsInfo });
};
export const getPostAction = ({ id }) => {
  return STATE_REDUCER(GET_POST_ACTION, { id });
};
export const updatePostAction = ({ id, title, body, userId }) => {
  STATE_REDUCER(UPDATE_POST_ACTION, { id, title, body });
};
export const addPostAction = ({ id, title, body, userId }) => {
  STATE_REDUCER(ADD_POST_ACTION, { id, title, body, userId });
};
const STATE_REDUCER = (action, props) => {
  switch (action) {
    case GET_USERS_ACTION:
      return STATE.users;
    case GET_POSTS_ACTION:
      return STATE.posts;
    case SET_USERS_ACTION:
      STATE.users = props.usersInfo;
      break;
    case SET_POSTS_ACTION:
      STATE.posts = props.postsInfo;
      break;
    case GET_POST_ACTION:
      return STATE.posts.find((post) => post.id === props.id);
    case UPDATE_POST_ACTION:
      const post = getPost(props.id).then((post) => {
        post.title = props.title;
        post.body = props.body;
      });
      break;
    case ADD_POST_ACTION:
      STATE.posts.unshift({
        id: props.id,
        title: props.title,
        body: props.body,
        userId: props.userId,
      });
  }
};
