import {
  getPostsAction,
  getUsersAction,
  setPostsAction,
  setUsersAction,
} from "./state.js";
import { fetchData, fetchJSON, wrapDataToPromise } from "./common.js";
const API = {
  users: "https://ajax.test-danit.com/api/json/users",
  posts: "https://ajax.test-danit.com/api/json/posts",
  post: (postId) => `https://ajax.test-danit.com/api/json/posts/${postId}`,
};
export const getUsers = () => {
  const users = getUsersAction();
  if (users instanceof Promise) {
    return users;
  } else if (users.length === 0) {
    setUsersAction({
      usersInfo: fetchJSON(API.users).then((json) => {
        setUsersAction({ usersInfo: json });
        return getUsersAction();
      }),
    });
  }
  return wrapDataToPromise(getUsersAction());
};

export const getPosts = () => {
  const posts = getPostsAction();
  if (posts instanceof Promise) {
    return posts;
  } else if (posts.length === 0) {
    setPostsAction({
      postsInfo: fetchJSON(API.posts).then((json) => {
        setPostsAction({ postsInfo: json });
        return getPostsAction();
      }),
    });
  }
  return wrapDataToPromise(getPostsAction());
};
export const setPost = ({ id, title, body, userId }) => {
  return fetchData(
    API.post(id),
    "PUT",
    JSON.stringify({ id, title, body, userId })
  ).then((response) => {
    if (response.ok) return response.json();
  });
};

export const createPost = ({ id, title, body, userId }) => {
  return fetchData(
    API.posts,
    "POST",
    JSON.stringify({ id, title, body, userId })
  ).then((response) => response.json());
};

export const deletePost = (postId) =>
  fetchData(API.post(postId), "DELETE").then((response) => response.ok);
