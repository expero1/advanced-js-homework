import { wrapDataToPromise } from "./common.js";
import { ErrorMessage } from "./elements/error-message.js";
import { NewPostButton } from "./elements/new-post-button.js";
import { Posts } from "./elements/posts.js";
import { RootElement } from "./elements/root-element.js";
import { ShowMessage } from "./elements/show-message.js";
import {
  AppError,
  catchAsyncError,
  handleError,
  postNotFoundError,
  userNotFoundError,
} from "./error.js";
import { CustomEvents } from "./events/events.js";
import { createPost, getPosts, getUsers, setPost } from "./remote-tasks.js";
import { addPostAction, getPostAction, updatePostAction } from "./state.js";

export const getUser = (userId) =>
  wrapDataToPromise(
    getUsers().then((users) => {
      const user = users.find((user) => user.id === Number(userId));
      if (user) return user;
      throw userNotFoundError(userId);
    })
  );

export const getPost = (id) =>
  getPosts().then(() => {
    const post = getPostAction({ id });
    if (post) return post;
    throw postNotFoundError(id);
  });
export const addPost = ({ id, title, body, userId }) =>
  createPost({ id, title, body, userId }).then(addPostAction);

export const updatePost = ({ id, title, body, userId }) =>
  setPost({ id, title, body, userId }).then(updatePostAction);

export const renderPosts = () => {
  document.body.querySelector(".root")?.remove();

  const rootElement = new RootElement();
  rootElement.show("body", "beforeend");
  const postsContainer = new Posts();
  rootElement.addElementToTree(postsContainer);
  const newPostButton = new NewPostButton();
  rootElement.addElementToTree(newPostButton, this, "afterbegin");
};
export const addEventListeners = () => {
  window.addEventListener(CustomEvents.eventNames.sendError, (e) => {
    handleError(
      new AppError(e.detail.message, e.detail.error, e.detail.context)
    );
  });

  window.addEventListener("unhandledrejection", (e) => {
    catchAsyncError(e);
  });
};
export const showMessage = (message) => {
  new ShowMessage(message);
};
export const showErrorMessage = (message) => {
  new ErrorMessage(message);
};
