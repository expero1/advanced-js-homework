import { fetchError, responseError } from "./error.js";

export const fetchData = (url, method = "GET", body = undefined) =>
  fetch(url, {
    method,
    body,
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((response) => {
      if (!response.ok) {
        response.text().then((responseBody) => {
          throw responseError(url, method, responseBody, response.status);
        });
      }
      return response;
    })
    .catch((error) => {
      throw fetchError(error, url);
    });

export const fetchJSON = (url) =>
  fetchData(url).then((response) => response.json());

export const wrapDataToPromise = (data) =>
  data instanceof Promise
    ? data
    : new Promise((r) => {
        r(data);
      });

export const waitDefferedTasks = (defferedTasks) => Promise.all(defferedTasks);
