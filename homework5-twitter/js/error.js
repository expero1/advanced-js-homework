import { CustomEvents } from "./events/events.js";
import { showErrorMessage } from "./tasks.js";

const ERROR_MESSAGE = {
  responseError: "Response error",
  fetchError: "Server not respond",
  statusError: "Server not respond",
  userNotFoundError: "User Not Found",
  postNotFoundError: "Post Not Found",
};

export class AppError extends Error {
  constructor(message, error, context) {
    super();
    this.name = "AppError";
    this.message = message;
    this.context = context;
    this.error = error;
    if (error?.stack) this.stack = error.stack;
  }
}

export const handleError = (error) => {
  showErrorMessage(error.message ?? error);
  console.log(error);
  console.log(`Context: ${JSON.stringify(error.context)}`);
};
export const catchAsyncError = (asyncErrorEvent) => {
  if (
    asyncErrorEvent instanceof PromiseRejectionEvent &&
    asyncErrorEvent.reason.name === "AppError"
  ) {
    asyncErrorEvent.preventDefault();
    const error = asyncErrorEvent.reason;
    CustomEvents.sendEvent(CustomEvents.eventNames.sendError, { ...error });
  }
};

export const fetchError = (error, url) => {
  return new AppError(ERROR_MESSAGE.fetchError, error, { url });
};
export const responseError = (url, method, responseBody, responseStatus) => {
  return new AppError(ERROR_MESSAGE.responseError, undefined, {
    url,
    method,
    responseBody,
    responseStatus,
  });
};
export const statusError = (error, url, status) => {
  return new AppError(ERROR_MESSAGE.statusError, error, { url, status });
};
export const userNotFoundError = (userId) => {
  return new AppError(ERROR_MESSAGE.userNotFoundError, undefined, { userId });
};
export const postNotFoundError = (postId) => {
  return new AppError(ERROR_MESSAGE.postNotFoundError, undefined, { postId });
};
