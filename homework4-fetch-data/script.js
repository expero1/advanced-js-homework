import { handleError } from "./js/error.js";
import {
  getCharactersContainerElement,
  createFilmCardElement,
  createLoader,
  rootElement,
  startEventListeners,
} from "./js/html-elements.js";
import { getCharacters, getFilms } from "./js/tasks.js";

const addCharactersToCard = ({ id: filmId, characters }) => {
  let charactersContainer = getCharactersContainerElement(filmId);
  const loader = createLoader("characters-loader");
  charactersContainer.before(loader);
  getCharacters(characters)
    .then((characters) => {
      characters.forEach((character) => {
        charactersContainer.append(character);
      });
    })
    .finally(() => {
      loader.remove();
    });
};
const createFilmCard = (filmInfo) => {
  const filmCardElement = createFilmCardElement(filmInfo);
  rootElement.append(filmCardElement);
  addCharactersToCard(filmInfo);
};
const addFilmsCardsToPage = () => {
  const loader = createLoader("page-loader");
  rootElement.append(loader);
  return getFilms()
    .then((films) => {
      films.forEach(createFilmCard);
    })
    .catch((error) => {
      handleError(error);
    })
    .finally(() => loader.remove());
};

startEventListeners();
addFilmsCardsToPage();

/*
Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

Технічні вимоги:

- Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films 
та отримати список усіх фільмів серії Зоряні війни

- Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. 
  Список персонажів можна отримати з властивості characters.
- Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. 
  Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
- Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, 
  вивести цю інформацію на екран під назвою фільму.

Необов'язкове завдання підвищеної складності

- Поки завантажуються персонажі фільму, прокручувати під назвою фільму анімацію завантаження. 
  Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.

Примітка
Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

*/
