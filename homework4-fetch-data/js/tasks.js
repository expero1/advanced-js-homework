import { fetchJSON, wrapDataToPromise, waitDefferedTasks } from "./common.js";
import { createCharactersCard } from "./html-elements.js";
const API = {
  COMMON: "https://ajax.test-danit.com/api/swapi",
  FILMS: `https://ajax.test-danit.com/api/swapi/films`,
  PEOPLE: "https://ajax.test-danit.com/api/swapi/people",
};
export const STATE = {
  FILMS: [],
  CHARACTERS: {},
  ERRORS: [],
};
window.STATE = STATE;

export const getFilms = () => {
  if (STATE.FILMS.length === 0) {
    STATE.FILMS = fetchJSON(API.FILMS).then((filmsInfo) => {
      STATE.FILMS = filmsInfo;
      return STATE.FILMS;
    });
  }
  return wrapDataToPromise(STATE.FILMS);
};

export const getCharacters = (characters) => {
  const charactersTasks = characters.map((url) => getCharacterInfo(url));
  return waitDefferedTasks(charactersTasks).then((charactersInfo) => {
    const characters = charactersInfo.map((characterInfo) =>
      createCharactersCard(characterInfo)
    );
    return characters;
  });
};
export const getCharacterInfo = (url) => {
  if (!STATE.CHARACTERS[url]) {
    STATE.CHARACTERS[url] = fetchJSON(url).then((characterInfo) => {
      STATE.CHARACTERS[url] = characterInfo;
      return characterInfo;
    });
  }
  return wrapDataToPromise(STATE.CHARACTERS[url]);
};
export const getFilmInfo = (filmId) => {
  return getFilms().then((filmsInfo) => {
    return filmsInfo.find(({ id }) => id === filmId);
  });
};
