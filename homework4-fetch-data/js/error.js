import { showModalMessage } from "./html-elements.js";

export const ERROR_MESSAGES = {
  RESPONSE_ERROR: "Response Error",
  CONNECTION_ERROR: "Connection Error",
};

export class AppError extends Error {
  constructor(message, reason, rawError) {
    super(message);
    this.name = "AppError";
    this.rawError = rawError;
    this.message = `"${this.name}: ${message}". Reason: ${reason}`;
    this.prettyMessage = `${message}`;
  }
}

export const handleError = (error) => {
  if (!(error instanceof AppError)) throw error;
  showModalMessage(error.prettyMessage ?? error.message);
  console.error(error);
  console.error(error.stack);
  return error;
};
