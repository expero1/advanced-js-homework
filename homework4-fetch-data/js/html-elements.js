export const rootElement = document.querySelector(".starwars");
export const getCharactersContainerElement = (filmId) =>
  document.querySelector(`[data-film-id='${filmId}'] .starwars__characters`);

const stringToHTMLElement = (HTMLString) => {
  const temporaryWrapper = document.createElement("div");
  temporaryWrapper.innerHTML = HTMLString;
  return temporaryWrapper.children[0];
};
export const createHTMLElement = ({
  tagName = "div",
  className = undefined,
  attr,
}) => {
  const HTMLEement = document.createElement(tagName);
  if (className) HTMLEement.className = className;
  if (attr) {
    for (let [attributeName, attributeValue] of Object.entries(attr)) {
      HTMLEement.setAttribute(attributeName, attributeValue);
    }
  }

  return HTMLEement;
};

export const createCharactersCard = (characterInfo) => {
  const characterCard = createHTMLElement({
    tagName: "li",
    className: "starwars-character",
  });
  characterCard.innerHTML = characterInfo.name;
  return characterCard;
};

export const createFilmCards = (filmsInfo) => {
  let filmsCardHTMLElements = [];
  for (let filmId in filmsInfo) {
    const filmCard = createFilmCard(filmsInfo[filmId]);
    filmsCardHTMLElements.push(filmCard);
  }
  return filmsCardHTMLElements;
};

export const createLoader = (className) => {
  const loaderElement = stringToHTMLElement(`<div class="load-spinner ${
    className ?? ""
  }">
  <div class="spinner-wrapper">
    <div><div></div></div>
    <div><div></div></div>
    <div><div></div></div>
    <div><div></div></div>
    <div><div></div></div>
    <div><div></div></div>
    <div><div></div></div>
    <div><div></div></div>
  </div>
</div>`);
  return loaderElement;
};
export const createFilmCardElement = ({
  id,
  episodeId,
  openingCrawl,
  name,
}) => {
  return stringToHTMLElement(`<div class="starwars-card" data-film-id="${id}">
    <h2 class="starwars-card__header">Star Wars Episode ${episodeId}. ${name}</h2>
    <ul class="starwars__characters"></ul>
    <div class="starwars-card__opening-crawl">${openingCrawl}</div>
  </div>`);
};
const renderModalWindow = () => {
  return stringToHTMLElement(`
<div class="modal-wrapper">
		<div class="modal-window">
			<div class="modal-content"></div>
			<button class="modal-close-btn">close</button>
		</div>
	</div>`);
};
const getModalWindow = () => {
  let modalWrapper = document.querySelector(".modal-wrapper");
  if (!modalWrapper) {
    modalWrapper = renderModalWindow();
    document.body.append(modalWrapper);
  }
  return modalWrapper;
};
export const startEventListeners = () => {
  const modalWrapper = getModalWindow();
  modalWrapper.addEventListener("click", ({ target }) => {
    if (
      !target.closest(".modal-window") ||
      target.closest(".modal-close-btn")
    ) {
      modalWrapper.classList.remove("active");
    }
  });
};

export function showModalMessage(message) {
  const modalWrapper = getModalWindow();
  modalWrapper.classList.add("active");
  modalWrapper.querySelector(".modal-content").innerText = message;
}
