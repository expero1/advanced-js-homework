import { handleError, AppError, ERROR_MESSAGES } from "./error.js";

const fetchData = (url) => {
  try {
    return fetch(url).catch((error) => {
      throw new AppError(ERROR_MESSAGES.CONNECTION_ERROR, error);
    });
  } catch {
    (error) => {
      throw new AppError(ERROR_MESSAGES.CONNECTION_ERROR, error);
    };
  }
};
export const fetchJSON = (url) => {
  return fetchData(url).then((response) => {
    if (response.ok) {
      return response.json();
    }
    throw new AppError(
      ERROR_MESSAGES.RESPONSE_ERROR,
      `response status:${response.status}, url:${url}`
    );
  });
};
export const wrapDataToPromise = (data) => {
  return data instanceof Promise ? data : new Promise((r) => r(data));
};

export const waitDefferedTasks = (defferedTasks) => {
  return Promise.allSettled(defferedTasks).then((result) => {
    const filteredResult = [];
    result.forEach(({ status: taskStatus, value, reason }) => {
      if (taskStatus === "rejected") {
        handleError(reason);
      } else {
        filteredResult.push(value);
      }
    });

    return filteredResult;
  });
};
