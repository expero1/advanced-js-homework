"use strict";
class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name;
  }
  set name(name) {
    this._name = name;
  }
  get age() {
    return this._age;
  }
  set age(age) {
    this._age = age;
  }

  get salary() {
    return this._salary;
  }
  set salary(salary) {
    this._salary = salary;
  }
}
class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }
  get salary() {
    return super.salary * 3;
  }
  set salary(salary) {
    super.salary = salary;
  }
}
const adam = new Programmer("Adam", 23, 100, ["js", "java", "c#"]);
const eve = new Programmer("Eve", 20, 110, ["c++", "js"]);
const arthur = new Programmer("Artur", 41, 99, ["python", "php"]);
// eve.age = 21;
// console.log(eve.salary);
// eve.salary = 111;
console.log(adam);
console.log(eve);
console.log(arthur);
/*
1. Створити клас Employee, у якому будуть такі характеристики - 
  name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
2. Створіть гетери та сеттери для цих властивостей.
3. Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
4. Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
5. Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.
*/
